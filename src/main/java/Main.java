import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class Main {
    public static void main(String[] args) {
        String file = "paths.txt";
        String file2 = "out.txt";
        int cores = Runtime.getRuntime().availableProcessors();
        final Object monitor = new Object();

        GetAllPaths getAllPaths = new GetAllPaths();
        GetResponse response = new GetResponse();
        SaveResponseInFile saveResponseInFile = new SaveResponseInFile();

        ExecutorService executorService = Executors.newFixedThreadPool((cores));

        for (String path : getAllPaths.run(file)) {
            synchronized (monitor) {
                executorService.submit(() -> saveResponseInFile.run(
                        file2,
                        response.run(path)
                ));
            }
        }
        try {
            executorService.shutdown();
            if (executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                System.out.println("Сканирование заголовков завершено!");
            } else {
                System.out.println("Сканирование не выполнено! Время ожидания истекло!");
            }
        } catch (Exception ex) {
            executorService.shutdownNow();
        }
    }
}
