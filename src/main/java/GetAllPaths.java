import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GetAllPaths {
    private final List<String> paths = new ArrayList<>();

    public List<String> run(String pathFile) {
        try (BufferedReader r = new BufferedReader(new FileReader(pathFile))) {
            while (true) {
                String line = r.readLine();
                if (line == null) break;
                paths.add(line);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return paths;
    }
}
