import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class SaveResponseInFile {

    public void run(String pathFile, String response) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(pathFile, true))) {
            writer.print(response + "\n");
            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
